const kftgLinks = [
  { text: 'Kanti Frauenfeld', href: 'https://www.kanti-frauenfeld.ch/' },
  {
    text: 'Stundenpläne',
    href: 'https://www.kanti-frauenfeld.ch/ueber-uns/stundenplaene.html/13550',
  },
  {
    text: 'Gymnasium',
    href: 'https://www.kanti-frauenfeld.ch/gymnasium.html/11476',
  },
  {
    text: 'Fachmittelschule',
    href: 'https://www.kanti-frauenfeld.ch/fachmittelschule.html/11477',
  },
  {
    text: 'Informatikmittelschule',
    href: 'https://www.kanti-frauenfeld.ch/informatikmittelschule.html/11479',
  },
  {
    text: 'Sonderaktivitäten IMS',
    href: 'https://www.kanti-frauenfeld.ch/informatikmittelschule/sonderaktivitaeten.html/11567',
  },
  {
    text: 'Fächer IMS',
    href: 'https://www.kanti-frauenfeld.ch/informatikmittelschule/faecher.html/11565',
  },
];

// Ihr Code hier
